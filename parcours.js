const lesParcours = [
    /*EXEMPLE D'UN PARCOURS
    {
        nom: "",
        temps: 0, //en minutes
        messageMauvaiseReponse: "",
        messageIndice: "",
        messageSuivant: "",
        messageEchec: "",
        etapes: [

            { //POUR LE DEBUT
                type: "debut",
                titre: "",
                synopsis: ""
            },

            { //QUESTION QUI NE SE PASSE PAS
                type: "code",
                titre: "",
                contexte: "",
                question: "",
                reponse: [""]
            },

            { //MESSAGE INFORMATIF
                type: "information",
                contenu: ""
            },

            { //QUESTION CLASSIQUE
                type: "question",
                titre: "",
                contexte: "",
                question:"",
                reponse: ["","",""],
                indice:["",""] // pas obligatoire
            },

            { //COFFRE CLASSIQUE
                type: "coffre",
                titre: "",
                contexte: "",
                reponse: [""]
            },

            { //QR CODE A SCANNER
                type: "qrcode",
                titre: "",
                contexte: "",
                reponse: [""]
            },

            { //COFFRE AVEC QR CODE DEDANS
                type: "qrcoffre",
                titre: "",
                contexte: "",
                reponse: [""]
            },

            { //POUR LA FIN
                type: "fin",
                titre: "",
                conclusion: ""
            }
        ],
    }
    */

    {
        nom: "Parcours tout public",
        temps: 150, // en minutes
        messageMauvaiseReponse:"Le gardien ne vous permet pas d'entrer une nouvelle réponse tout de suite… patientez !",
        messageIndice:"Le gardien d'âmes vous donne un indice…",
        messageSuivant:"Dans sa grande mansuétude le gardien décide de vous envoyer vers la prochaine étape…",
        messageEchec:"Vulnerant omnes, ultima necat. Chaque heure nous meurtrit, la dernière tue. Le meilleur de vos âmes sera bientôt vendu sur le Marché Spirituel d'Atcitloa… fin du jeu.",
        etapes: [
            {
                type: "debut",
                titre:"Introduction",
                synopsis: "Nous sommes en 2050. Bienvenue à Atcitloa, la cité des vendeurs d'âme ! Vous avez cru pouvoir vous y promener  tranquillement et vos âmes ont été piégées dans trois coffres-prisons. Vous voici donc à Atcitloa en 2020, à l'époque où elle s'appelait La Ciotat. A vous de retrouver les clés d'espérance, de confiance et de résolution qui seules vous permettront de conserver votre pouvoir d'agir une fois les deux heures et demi écoulées. Sinon, vos âmes seront la proie des vendeurs d'âme d'Atciltloa… Frédéric le gardien sera impitoyable."
            },

            {
                type: "code",
                titre: "L'épreuve du gardien",
                contexte: "Pour commencer, entrez le code d'activation donné par le gardien.",
                reponse: ["au pole nord 13600"]
            },

            {
                type: "code",
                titre: "Épreuve 1",
                contexte: "Rendez-vous devant la fleuriste du bas de la rue des Poilus, trouvez l’indice !",
                question:"Notez la locution latine de la fleuriste SAN VICENTE :",
                reponse: ["absentem laedit, qui cum ebrio litigat"]
            },

            {
                type: "information",
                contenu: "Eh oui, « Celui qui se querelle avec un ivrogne frappe un absent. »"
            },

            {
                type: "question",
                titre: "Épreuve 2",
                contexte: "Dans Atcitloa, méfiez-vous, la police n’est pas nécessairement votre amie comme dans notre monde de 2050. Aussi les autorités ont-elles placé une autorité supérieure sur la ville. Regardez la statue que vous pouvez admirer à un étage de l’angle que fait la rue des Poilus avec la rue Edgar Quinet.",
                question:"À quel étage cette statue est-elle située ?",
                reponse: ["3","3eme","3eme etage"]
            },

            {
                type: "question",
                titre: "Épreuve 3",
                contexte: "En l'observant bien, cette statue vous semble familière, peut-être connaissiez-vous la personne représentée…",
                question: "Que représente la statue ?",
                reponse: ["sainte vierge", "vierge marie", "vierge"]
            },

            {
                type:"information",
                contenu:"Bravo, la sainte-Vierge d’Atcitloa veuille sur vous ! Elle vous donne rendez-vous devant l’église Notre-Dame de l’Assomption, en haut des marches des escaliers dominant le port-vieux. Eh oui ! A Atcitloa, on dit Port-vieux alors qu’à Marseille, on parle du Vieux-Port. Mais comme vous le savez, l’important est d’être jeune dans son âme. Conservez précieusement en mémoire la lettre P."
            },

            {
                type: "question",
                titre: "Épreuve 4",
                contexte: "Dans Atcitloa, grande est la puissance de la religion. Le 15 Août, il y a procession et le curé de la paroisse bénit les bateaux en mer. L’église ND de l’assomption date du début du 17ème siècle.",
                question: "Que voyez-vous dans l’alcôve au-dessus de la porte d’entrée : Marie et l’ange Gabriel / Marie et Anne / Marie et Jésus / Marie, Jésus et Joseph / Marie et Marie-Madeleine ?",
                reponse: ["marie et jesus"]
            },

            {
                type: "question",
                titre: "Épreuve 5",
                contexte: "",
                question: "Qui soutient cette église ?",
                reponse: ["echine grise"],
                indice: ["Ça rime.", "Les trois dragons de La Ciotat."]
            },

            {
                type: "code",
                titre: "Épreuve 6",
                contexte: "Rendez-vous place Sadi Carnot (derrière l’église) pour chercher l’indice à la fontaine.",
                question: "Notez la locution latine de La fontaine :",
                reponse: ["bona valetudo melior est quam maximae divitiae"]
            },

            {
                type:"information",
                contenu:"« Une bonne santé vaut mieux que les plus grandes richesses. » La fontaine est souvent source de sagesse, n’est-ce pas ?"
            },

            {
                type: "question",
                titre: "Épreuve 7",
                contexte: "En faisant le tour de l’église par la place Sadi Carnot, vous en trouverez une (une tour !) bien plus vieille puisqu’elle abritait la sacristie de la chapelle primitive du Moyen-Age. Comptez le nombre d’ouvertures de la vieille « torre de la gleiso » rue de la Calade, ainsi que le nombre de trous pratiqués dans la porte de la sacristie. Additionnez ces deux chiffres qui vous permettront de trouver la lettre correspondant à ce rang dans l’alphabet.",
                question: "De quelle lettre s'agit-il ?",
                reponse: ["x"]
            },

            {
                type:"information",
                contenu:"Bravo ! Le X, signe du mystère s'il en est, vous sera précieux. Gardez-le en mémoire ou sur un document quelconque. A Atcitloa, la plupart des habitants du centre-ville signent d'un X, preuve d'appartenance à la guilde du Monde Impermanent. Descendez la rue de la calade et, sur le quai, prenez à gauche."
            },

            {
                type: "question",
                titre: "Épreuve 8",
                contexte: "Rendez-vous aux saveurs de l’est.",
                question: "Notez la date du tribunal de pêche :",
                reponse: ["1760"],
                indice: ["L'est ne serait-il pas l’orient ?", "Cherchez la statue au-dessus du restaurant « les saveurs de l’orient »."]
            },

            {
                type: "question",
                titre: "Épreuve 9",
                contexte: "Bravo pour votre hauteur de vue ! Poursuivez jusqu’au dernier immeuble jouxtant le rond-point, le Musée ciotaden qui fait face à l’office du tourisme de La Ciotat. A Atcitloa, c’est le Rendez-Vous des bons offices !",
                question: "À votre avis, à quel le époque remonte la transformation de la crique naturelle en port (préhistoire, antiquité ou Moyen-Age) ?",
                reponse: ["antiquite"]
            },

            {
                type: "question",
                titre: "Épreuve 10",
                contexte: "Oui, suite à l'annexion de Massalia en 49 avant JC (toute la Gaule n'était pas encore occupée ), les Romains renommèrent le comptoir grec de Kitharistès en latin, Citharista, avec un port à l'emplacement du port-vieux actuel mais d'une surface et d'une profondeur bien moindres.",
                question: "D'après vous, quelle fonction occupait ce musée autrefois : la mairie, la direction des chantiers navals, l’hôtel particulier de la famille Grimaldi ou le siège de l’amirauté ?",
                reponse: ["mairie"]
            },

            {
                type:"information",
                contenu:"Bravo ! Les âmes des anciens maires veillent à présent sur vous. Gardez précieusement avec vous la lettre M."
            },

            {
                type: "question",
                titre: "Épreuve 11",
                contexte: "Dirigez-vous vers l'Office du Tourisme, de l'autre côté du Boulevard Anatole France, appelé également « La tasse » par les anciens.",
                question: "À votre avis, à quel mot correspondait la « tasse » ?",
                reponse: ["taxe"],
                indice: ["Remplacez les deux s par une seule consonne", "Cette lettre, vous l’avez déjà gardée en mémoire"]
            },

            {
                type:"information",
                contenu:"Bravo, les âmes des anciens payeurs de taxe et autres octrois veillent sur vous ! Gardez précieusement avec vous la lettre T."
            },

            {
                type: "question",
                titre: "Épreuve 12",
                contexte: "",
                question: "Quelles sont les langues parlées à l’office du tourisme (à inscrire dans l’ordre, juste séparées par un espace) ?",
                reponse: ["français anglais italien espagnol armenien thailandais "],
            },

            {
                type:"information",
                contenu:"Tant de langues, tant de langues ! Quand je pense qu’en Atcitloa, il ne subsiste plus que le Néolatin pour l’administration Interplanétaire et l’Espéranto Nuevo comme langue de tous les jours. Félicitations ! Tentez dès à présent de trouver la clé d’espérance pour ouvrir le premier coffre. Il vous suffit de retourner chez le gardien et de lui présenter une phrase écrite avec uniquement des mots commençant par les 4 lettres que vous avez récoltées."
            },

            {
                type: "coffre",
                titre: "Premier coffre",
                contexte: "Bien joué vous avez trouvé le premier coffre ! Quel est l'indice qui s'y trouve ?",
                reponse: ["jean-vincent verdonnet"]
            },

            {
                type: "question",
                titre: "Épreuve 13",
                contexte: "Grand poète français du XXème siècle, Jean-Vincent Verdonnet fut un des premiers à encourager Frange d’Ag.",
                question: "Que conseille le poète ainsi nommé ?",
                reponse: ["espere et tremble"],
            },

            {
                type: "code",
                titre: "Épreuve 14",
                contexte: "Vous valez bien mieux que des citoyens de troisième zone. En piste pour trouver le deuxième coffre ! Le prochain indice se trouve au 52 de la rue des Poilus.",
                question: "Notez la locution latine du 52 rue des Poilus :",
                reponse: ["intelligenti pauca"],
            },

            {
                type:"information",
                contenu:"« À ceux qui comprennent, peu de mots suffisent. » Cette locution pourrait être celle des sa-vants et des poètes."
            },

            {
                type: "question",
                titre: "Épreuve 15",
                contexte: "Félicitations, vos âmes commencent à être sauvées ! Bien sûr, vous garderez des séquelles si vous n’arrivez pas jusqu’au deuxième coffre, mais au moins votre intégrité spirituelle est-elle préservée. Pour commencer vos recherches vous menant à la clé du 2ème coffre, vous devez vous rendre près d’une portion infime de l’âme d’un grand homme venu à la Ciotat. Rendez-vous au 58 de la rue des Poilus.",
                question: "De qui s'agit-il ?",
                reponse: ["napoleon bonaparte","napoleon", "bonaparte"],
            },

            {
                type: "question",
                titre: "Épreuve 16",
                contexte: "",
                question: "En quelle année ?",
                reponse: ["1793"],
            },

            {
                type: "question",
                titre: "Épreuve 17",
                contexte: "",
                question: "Combien de nuits le capitaine d’artillerie Bonaparte dormit-il ici ?",
                reponse: ["2", "deux"],
            },

            {
                type:"information",
                contenu:"Oui ! Une fraction d’âme de Bonaparte vous accompagne désormais dans votre quête. Montez la rue des Poilus, prenez à droite et dirigez-vous à présent au sommet des escaliers de la place du 8 mai 1945, devant l’entrée principale de la chapelle des Pénitents bleus. Gardez bien en tête la lettre B."
            },

            {
                type: "question",
                titre: "Épreuve 18",
                contexte: "Cette splendide chapelle fut construite au début du XVIIème siècle. Les confréries de Pénitents étaient des regroupements de pieux laïques louant Dieu publiquement et accomplissant des œuvres charitables.",
                question: "Pour vous gagner les âmes de tous les Pénitents, écrivez quelles étaient les couleurs des trois confréries de pénitents de La Ciotat (bleu blanc rouge / bleu blanc noir / bleu blanc gris / bleu blanc vert)",
                reponse: ["bleu blanc noir"],
                indice: ["Ni rouge ni vert."],
            },

            {
                type: "question",
                titre: "Épreuve 19",
                contexte: "En haut de la façade de la chapelle des Pénitents bleus se trouve un mot.",
                question: "Quel est ce mot ?",
                reponse: ["mar"]
            },

            {
                type:"information",
                contenu:"Comme en espagnol, mar signifie mer en Provençal. Que les âmes des Pénitents veillent toujours sur vous ! Gardez bien la lettre C (comme chapelle) avec vous ! Rendez-vous à présent en bas des escaliers, traversez la rue et approchez-vous du taureau."
            },

            {
                type: "question",
                titre: "Épreuve 20",
                contexte: "Ce taureau de cinéma est là pour faire rire et sourire les gens, mais vous devez savoir qu’il est en réalité l’âme du Biòu de mar, le taureau de mer, qui est à l’origine de la baie de La Ciotat et qui vient soulever violemment les vagues lors de l’équinoxe d’automne.",
                question: "Selon la légende, par qui aurait-il été apporté, les Phéniciens, les Grecs, les Maures ou les Romains ?",
                reponse: ["pheniciens"]
            },

            {
                type: "question",
                titre: "Épreuve 21",
                contexte: "Osco, bravi gens ! Bravo ! Pour obtenir le soutien de l’âme énergique du Biòu de mar, promenez-vous le long des pannes du port de plaisance.",
                question: "Inscrivez le nombre correspondant à la panne maximale :",
                reponse: ["1000"]
            },

            {
                type:"information",
                contenu:"L’âme du taureau de mer est en vous. Gardez bien en tête la lettre T et approchez-vous d’un lieu mythique que le monde terrestre entier nous envie : l’Eden-théâtre du 25 Boulevard Georges Clémenceau (proche du Biòu de mar)."
            },

            {
                type: "question",
                titre: "Épreuve 22",
                contexte: "Cet édifice est le plus vieux cinéma du monde encore en activité.",
                question: ". Mais quels sont donc les deux mots grecs qui ont présidé à la naissance du mot cinématographe?",
                reponse: ["kinema et graphein", "kinema graphein"],
                indice: ["Ce sont deux mots grecs.","Vous trouverez ces renseignements au chapitre « cinéma » de votre livret."]
            },

            {
                type: "code",
                titre: "Épreuve 23",
                contexte: "Les mânes des pionniers du cinéma, de Jansen aux frères Lumière, vous soutiennent à présent. Remontez pour chercher l’indice dans un commerce qui miroite proche de la place des maquisards, en haut de la rue des Poilus.",
                question: "Notez la locution latine 4 :",
                reponse: ["et suppositio nil ponit in esse"]
            },

            {
                type:"information",
                contenu:"Sacrés Romains, ils connaissaient déjà les infox ! « Supposer une chose ne lui confère pas l'existence. »"
            },

            {
                type: "question",
                titre: "Épreuve 24",
                contexte: "Dirigez-vous vers le jardin de la ville et lisez les inscriptions sous chacune des statues. Cherchez le nom de l’animal figurant sur une des statues.",
                question: "Inscrivez ce nom et le nombre de statues séparés par un espace (exemple : Juju 9) :",
                reponse: ["zaza 4"]
            },

            {
                type: "question",
                titre: "Épreuve 25",
                contexte: "",
                question: "Quel est le nom du généreux donateur qui a financé les quatres statues du jardin ?",
                reponse: ["barbaroux", "albert barbaroux"]
            },

            {
                type:"information",
                contenu:"Albert Barbaroux, Ciotaden mythique, amateur de belles voitures, ami de Michel Simon. Puisse son âme vous guider vers le deuxième coffre ! Les âmes des légendes de La Ciotat sont avec vous. Gardez bien en tête la lettre L et dirigez-vous à présent, en sortant du jardin, sur la droite."
            },

            {
                type: "code",
                titre: "Épreuve 26",
                contexte: "Descendez l’avenue de la République (c’est long) et tournez à gauche sur la place Esquiros où vous trouverez l’indice suivant dans un restaurant italien.",
                question: "Notez la locution latine du restaurant italien :",
                reponse: ["felix qui potuit rerum cognoscere causas"]
            },

            {
                type:"information",
                contenu:"« Heureux celui qui a pu pénétrer le fond des choses. » Vous approchez lentement du but…"
            },

            {
                type: "question",
                titre: "Épreuve 27",
                contexte: "Vous vous rapprochez du deuxième coffre. Sentez-vous vos âmes frémir ? ",
                question: "Quels sont les noms des deux groupes imprégnés de spiritualité qui sont à l’origine des deux chapelles : Les Pénitents bleus et les Pénitents noirs, les Pénitents bleus et les Servites, Les Pénitents blancs et les Oratoriens, Les Pénitents noirs et les Servite ou Les Minimes et les Pénitents blancs ?",
                reponse: ["les penitents noirs et les servites"]
            },

            {
                type: "question",
                titre: "Épreuve 28",
                contexte: "Les Servites gardaient la source sainte (fons sanctus en latin, font santo en provençal, d’où le mot Fontsainte aujourd’hui).",
                question: "Quelle est le nom de l'école qui jouxte la chapelle Saint-Joseph ?",
                reponse: ["sainte anne", "sainte-anne", "st anne"]
            },

            {
                type:"information",
                contenu:"La seule école avec un enseignement religieux à La Ciotat vous guide. Gardez bien en tête la lettre S (comme Servites) et retournez voir le gardien en lui montrant un beau texte que vous aurez écrit avec tout ou partie des premiers et derniers mots des vers (lignes poétiques) des deux poèmes du livret de jeu."
            },

            {
                type: "coffre",
                titre: "Deuxième coffre",
                contexte: "Bien joué vous avez trouvé le deuxième coffre ! Quel est l'indice qui s'y trouve ?",
                reponse: ["lampe au neon pelle a tarte"]
            },

            {
                type: "question",
                titre: "Épreuve 29",
                contexte: "Super, vous êtes en possession du deuxième indice !",
                question: "Que demande l’indice du 2ème coffre à la masco de Figuerolles ?",
                reponse: ["enchantements et mysteres"]
            },

            {
                type: "question",
                titre: "Épreuve 30",
                contexte: "Enchantez vos mystères !",
                question: "Où apportaient-ils leurs écus et d'aitres choses encore ?",
                reponse: ["sous le pin de la fade"]
            },

            {
                type:"information",
                contenu:"La masco (la sorcière) et la fado (la fée) soient avec vous ! A présent, concentrez-vous sur la recherche du TROISIEME COFFRE."
            },

            {
                type: "question",
                titre: "Épreuve 31",
                contexte: "Rendez-vous au bas du Boulevard Gueymard.",
                question: "Indiquez le noms des deux bars, l'un en français, l'autre en anglais (exemple: le central et the champion) :",
                reponse: ["le cristal et the crown"]
            },

            {
                type: "question",
                titre: "Épreuve 32",
                contexte: "Vite, le temps presse pour récupérer la dernière partie de vos âmes ! Allez sur le port-vieux et cherchez près d’une grue le monument aux victimes de l’amiante.",
                question: "Quelle date devez-vous vénérer pour que les âmes des victimes (et on meurt encore aujourd’hui de l’amiante à La Ciotat) vous suivent ?",
                reponse: ["28 avril"]
            },

            {
                type:"information",
                contenu:"Soyez sûr-e-s que les âmes des victimes de l’amiante vous aideront à présent dans votre quête ultime. "
            },

            {
                type: "question",
                titre: "Épreuve 33",
                contexte: "Vous observez de nouveau le monument.",
                question: "Mais qui est son créateur ?",
                reponse: ["gilber ganteaume", "ganteaume"]
            },

            {
                type:"information",
                contenu:"Gilbert Ganteaume, ancien salarié des chantiers navals et artiste, a plusieurs de ses œuvres à La Ciotat, dont le fameux tryptique ornant les murs de l’église ND de l’assomption. Une ode aux chantiers à voir absolument ! Soyez sûrs du soutien des âmes des victimes de l’amiante et conservez précieusement la lettre A ! Remontez la rue Bouronne puis le Boulevard Bertolucci et cherchez la stèle devant la mairie."
            },

            {
                type: "question",
                titre: "Épreuve 34",
                contexte: "Remontez la rue Bouronne puis le Boulevard Bertolucci et cherchez la stèle devant la mairie.",
                question: "Ce monument est dédié aux chômeurs/travailleurs/élus/résistants/déportés :",
                reponse: ["chomeurs"]
            },

            {
                type:"information",
                contenu:"Les âmes des chômeurs veillent à présent sur vous. Gardez la lettre P (comme Pôle Emploi) en tête."
            },

            {
                type: "question",
                titre: "Épreuve 35",
                contexte: "",
                question: "Avant de devenir l’hôtel de ville, ce bâtiment abritait : la direction des chantiers navals, le foyer des anciens des chantiers navals, la capitainerie, le lycée Lumière ou le collège Jean Jaurès ?",
                reponse: ["direction des chantiers navals"],
                indice: ["La réponse se trouve dans le livret."]
            },

            {
                type:"information",
                contenu:"Les âmes des anciens dirigeants des chantiers veillent à présent sur vous. Gardez la lettre C en tête."
            },

            {
                type: "question",
                titre: "Épreuve 36",
                contexte: "Remontez la rue Bouronne puis le Boulevard Bertolucci et approchez de la statue qui se trouve près de l’angle avec la rue Emile Delacour et du Boulevard Michelet.",
                question: "Comment se nomme le « héros » dont le buste orne l’endroit ?",
                reponse: ["marc henri victor delacour", "delacour victor marc henri"],
                indice: ["Indiquez bien les trois prénoms et le nom !"]
            },

            {
                type:"information",
                contenu:"Delacour fut le fondateur de la cité ouvrière de La Ciotat. Bâtie entre 1854 et 1856, elle fut l’une des premières cités ouvrières de France à proximité des ateliers. La cité Notre-Dame-des-Victoires s’étendait sur près de 21.000 m2 et pouvait accueillir près de 800 personnes. Elle comportait 24 maisons de 8 logements."
            },

            {
                type: "question",
                titre: "Épreuve 37",
                contexte: "Pour bénéficier du soutien des âmes des anciens habitants de la cité, dites-nous quel fut le nom de sa troisième et dernière rue (avec les rues Victor Delacour et Notre-Dame des Victoires).",
                question: "Rue des ouvriers, rue des chantiers, rue des collines, rues des arbres ou rue des machines ?",
                reponse: ["rue des arbres"],
                indice: ["La réponse est dans le livret.", "Cherchez à la page « les chantiers »."]
            },

            {
                type:"information",
                contenu:"Bravo ! les âmes de la cité veillent sur vous et votre quête. Gardez bien le O d’ouvrier en votre mémoire fertile !"
            },

            {
                type: "code",
                titre: "Épreuve 38",
                contexte: "Allez jusqu’à la boulangerie du Boulevard Bertolucci pour trouver l’indice suivant.",
                question: "Notez la locution latine de la BOULANGERIE TOMAS :",
                reponse: ["hic sunt dracones"]
            },

            {
                type: "information",
                contenu: "« Ici sont des dragons. » Vous avez certainement traduit par vous-mêmes. Ce terme signifie qu’ici se trouve un territoire peuplé de dangers, mais ce n’est pas le cas de la boulangerie. Peut-être un danger vous menace-t-il à proximité ? Soyez sur vos gardes !"
            },

            {
                type: "information",
                contenu: "Empruntez le boulevard Michelet jusqu’à ce que vous repériez sur votre gauche l’entrée du cimetière Sainte-Croix. N’entrez pas dans le cimetière, restez au niveau de l’entrée."
            },

            {
                type: "question",
                titre: "Épreuve 39",
                contexte: "Observez les deux panneaux de part et d’autre de l’allée centrale qui mène au monument des résistants et multipliez les deux chiffres qui y figurent.",
                question: "Vous obtenez le nombre :",
                reponse: ["180"]
            },

            {
                type: "question",
                titre: "Épreuve 40",
                contexte: "",
                question: "Pour obtenir l’accord des âmes de tous les défunts du cimetière Sainte-Croix, indiquez la devise inscrite sur le monument des résistants (visible au bout de l’allée centrale), lisible depuis l’entrée.",
                reponse: ["pour la france jusqu'au bout"]
            },

            {
                type: "information",
                contenu: "C’est un grand soutien que cette multitude d’âmes vous apporte ! Redescendez sur le Boulevard Michelet et prenez à gauche. Arrêtez-vous lorsque vous verrez une chapelle sur votre gauche. Et que le D des défunts vous protège ! Gardez bien cette lettre en tête."
            },

            {
                type: "question",
                titre: "Épreuve 41",
                contexte: "L’œuvre de jeunesse abritait en son temps le quatrième cinéma de La Ciotat. Afin de sentir derrière vous l’allant de toutes les âmes des personnes passées par le patronage.",
                question: "Inscrivez le nom de l’association qui est aujourd’hui aux commandes de ce lieu.",
                reponse: ["vaillante"]
            },

            {
                type: "information",
                contenu: "Comment vous féliciter ? Les âmes de la jeunesse vous suivront jusqu’au bout ! Emportez dans les puissants sillons de votre cerveau la lettre V. A présent, vous allez marcher un peu. Prenez à droite en continuant à longer le mur de l’œuvre de Jeunesse, puis tournez à gauche dans l’avenue de la pétanque. Au bout de celle-ci, prenez à droite comme si vous contourniez le stade Masse. Empruntez le trottoir de gauche et approchez-vous de la petite statue qui garde l’entrée de l’endroit le plus légendaire de La Ciotat."
            },

            {
                type: "question",
                titre: "Épreuve 42",
                contexte: "Savourons cet instant malgré la proximité de la clé de la résolution ! Ici eut lieu l’invention d’un jeu que les mondes entiers nous envient : la pétanque. Admirez la flexion des genoux, le soupeser de la main, la grâce des avant-bras ! Et, plus que tout, la précision de chaque geste…",
                question: "En quelle année la pétanque fut-elle inventée ?",
                reponse: ["1910"]
            },

            {
                type: "question",
                titre: "Épreuve 43",
                contexte: "",
                question: "En quel mois ?",
                reponse: ["juin"]
            },

            {
                type: "question",
                titre: "Épreuve 44",
                contexte: "",
                question: "Quelle autre année est-elle invoquée quelquefois ?",
                reponse: ["1907"]
            },

            {
                type: "question",
                titre: "Épreuve 45",
                contexte: "",
                question: "De quelle couleur est la statue du joueur de pétanque ?",
                reponse: ["noir"]
            },

            {
                type: "information",
                contenu: "Que peut-il vous arriver, à présent que les âmes de tous les pétanqueurs et de toutes les pétanqueuses du monde sont avec vous ? Le temps, peut-être…retournez sur l’avenue de la pétanque, descendez-la, traversez le Boulevard Michelet et suivez l’avenue du maréchal Gallieni jusqu’à l’indice qui se trouve près d’une boutique de pompes funèbres. Conservez précieusement la lettre L en hommage à Jules Le Noir !"
            },

            {
                type: "code",
                titre: "Épreuve 46",
                contexte: "",
                question: "Notez la locution latine de la boutique de pompes funèbres:",
                reponse: ["de mortuis aut bene aut nihil"]
            },

            {
                type: "information",
                contenu: "« Des morts, on dit du bien ou on se tait. » Georges Brassens chantait « les morts sont tous de braves types. » Mais les vivants, méfiez-vous en !"
            },

            {
                type: "code",
                titre: "Épreuve 47",
                contexte: "Poursuivez la descente de l’avenue Gallieni sur quelques mètres jusqu’au monument à la mémoire des déportés à l’angle de l’avenue Louis crozet.",
                question: "Le code angélique devrait vous aider à déchiffrer le message du monument aux déportés…",
                reponse: ["allee doree amen ou sires"]
            },

            {
                type: "information",
                contenu: "Vous êtes presque au bout de vos peines. La libération est proche. Les âmes des déportés vous guideront jusqu’à la victoire finale avec la lettre F. Trouvez l’indice à la boucherie de la place Evariste Gras."
            },

            {
                type: "code",
                titre: "Épreuve 48",
                contexte: "",
                question: "Notez la locution latine de la boucherie : ",
                reponse: ["libido sciendi"]
            },

            {
                type: "information",
                contenu: "« Désir de savoir. » Certainement, ce désir vous a permis de vous rapprocher sûrement du troisième et dernier coffre. Rendez-vous sur la place Evariste Gras devant le cinéma Lumière."
            },

            {
                type: "question",
                titre: "Épreuve 49",
                contexte: "Deux catégories d’âmes ici nous intéressent : celles des admirateurs des frères Lumière et celles des admirateurs de Gustave Eiffel lui-même, puisque l’ingénieur marseillais Delestrade s’inspira du plus célèbre monument français pour construire le marché couvert en 1892, dont l’aile sud devint cinéma en 1913. Allez derrière le cinéma Lumière et additionnez tous les chiffres que vous trouverez sur la façade.",
                question: "Quel nombre obtenez-vous ? ",
                reponse: ["89"]
            },

            {
                type: "information",
                contenu: "« Désir de savoir. » Certainement, ce désir vous a permis de vous rapprocher sûrement du troisième et dernier coffre. Rendez-vous sur la place Evariste Gras devant le cinéma Lumière."
            },

            {
                type: "code",
                titre: "Épreuve 50",
                contexte: "Ce chiffre est mythique, puisqu’il et celui de la révolution française. Volez à présent jusqu’au coffre final avec une phrase ! Mais pas n’importe quelle phrase. Prenez dans l’ordre toutes les lettres que vous avez trouvées depuis le début du jeu. Chaque lettre sera la première d’un mot. Tous ces mots mis bout à bout doivent former une phrase compréhensible. Frédéric le gardien vous attend !",
                question: "Entrez le code de fin de jeu qui arrêtera le temps et vous fera peut-être entrer dans les meilleurs scores !",
                reponse: ["quand vous aimez le sable a fui 74380"]
            },

            {
                type: "fin",
                titre: "Fin du parcours",
                conclusion: "Vous avez réussi ! Vos âmes sont sauvées ! Vous avez à présent accès au téléporteur temporel qui vous permettra de retrouver Atcitloa. Citoyens interplanétaires, bon retour, padon, bonum reditum ad Atcitloa !"
            }
        ]
    },

]